import spacy


def convert_tokenid(filename):
    nlp = spacy.load('en_core_web_md')
    text = open(filename, 'r').read()

    doc = nlp(text)

    ids = []
    for token in doc:
        if token.has_vector:
            id = nlp.vocab.vectors.key2row[token.norm]
        else:
            id = None
        ids.append(id)

    print([token for token in doc])
    print(ids)


filename = 'Dlab_environmentalScience.txt'
convert_tokenid(filename)