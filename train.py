from transformers import (BloomTokenizerFast,
                          BloomForTokenClassification,
                          DataCollatorForTokenClassification, 
                          AutoModelForTokenClassification, 
                          TrainingArguments, Trainer)
from datasets import load_dataset
import torch
import os

model_name = "bloom-560m"
tokenizer = BloomTokenizerFast.from_pretrained(f"bigscience/{model_name}", add_prefix_space=True)
model = BloomForTokenClassification.from_pretrained(f"bigscience/{model_name}")
datasets = load_dataset('conll2003')
global mpdel

example = datasets["train"][100]
tokenized_input = tokenizer(example["tokens"], is_split_into_words=True)
tokens = tokenizer.convert_ids_to_tokens(tokenized_input["input_ids"])


def tokenizeInputs(inputs):
      
    tokenized_inputs = tokenizer(inputs["tokens"], max_length = 512, truncation=True, is_split_into_words=True)
    word_ids = tokenized_inputs.word_ids()
    input_ids = tokenized_inputs["input_ids"]
    labels = [input_ids[word_id] for word_id in word_ids]
    tokenized_inputs["labels"] = labels
    print(tokenized_inputs['input_ids'])
    return tokenized_inputs



example = datasets["train"][100]
tokenizeInputs(example)
tokenized_datasets = datasets.map(tokenizeInputs)
tokenized_datasets = tokenized_datasets.remove_columns(["id", "tokens", "ner_tags", "pos_tags", "chunk_tags"])
data_collator = DataCollatorForTokenClassification(tokenizer=tokenizer)
model = AutoModelForTokenClassification.from_pretrained(f"bigscience/{model_name}", num_labels=12).cuda()
# Estimate FLOPS needed for one training example
sample = tokenized_datasets["train"][0]
sample["input_ids"] = torch.Tensor(sample["input_ids"])
flops_est = model.floating_point_ops(input_dict = sample, exclude_embeddings = False)

training_args = TrainingArguments(
    output_dir="./results",
    save_strategy= "epoch", # Disabled for runtime evaluation 
    evaluation_strategy="steps", #"steps", # Disabled for runtime evaluation 
    eval_steps = 500,
    learning_rate=2e-5,
    per_device_train_batch_size=12,
    per_device_eval_batch_size=12,
    num_train_epochs=2,
    weight_decay=0.01,
    report_to="none",
    #fp16=True,
)

trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=tokenized_datasets["train"],
    eval_dataset=tokenized_datasets["test"],
    tokenizer=tokenizer,
    data_collator=data_collator,
)
trainer.train()