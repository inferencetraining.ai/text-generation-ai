import torch
from transformers import BloomTokenizerFast, BloomForSequenceClassification
from transformers import AdamW
from datasets import load_dataset

import transformers
from transformers import BloomForCausalLM
from transformers import BloomTokenizerFast


tokenizer = BloomTokenizerFast.from_pretrained("bigscience/bloom-560m")
model = BloomForSequenceClassification.from_pretrained("bigscience/bloom-560m", problem_type="multi_label_classification")
datasets = load_dataset('conll2003')
example = datasets["train"][100]

inputs = tokenizer("Hello, my dog is cute", return_tensors="pt")

with torch.no_grad():
    logits = model(**inputs).logits

predicted_class_id = logits.argmax().item()
model.config.id2label[predicted_class_id]

num_labels = len(model.config.id2label)
model = BloomForSequenceClassification.from_pretrained(
    "bigscience/bloom-560m", num_labels=num_labels, problem_type="multi_label_classification"
)

labels = torch.nn.functional.one_hot(torch.tensor([predicted_class_id]), num_classes=num_labels).to(
    torch.float
)
optimizer = AdamW(model.parameters(), lr=1e-5)
loss = model(**inputs, labels=labels).loss
loss.backward()
optimizer.step()



#torch.save(model.state_dict(), '/home/ubuntu/text-generation-ai/test.pth')
mmodel = torch.load('/home/ubuntu/text-generation-ai/test.pth')
print(help(model))