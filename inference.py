import transformers
from transformers import BloomForCausalLM
from transformers import BloomTokenizerFast
import torch

import sys

args = sys.argv

model = BloomForCausalLM.from_pretrained("bigscience/bloom-1b3")
tokenizer = BloomTokenizerFast.from_pretrained("bigscience/bloom-1b3")

prompt = args[1]
result_length = args[2]
print(prompt, result_length)
#inputs = tokenizer(prompt, return_tensors="pt")

# Beam Search
print(tokenizer.decode(model.generate(inputs["input_ids"],
                       max_length=result_length, 
                       num_beams=2, 
                       no_repeat_ngram_size=2,
                       early_stopping=True
                      )[0]))
